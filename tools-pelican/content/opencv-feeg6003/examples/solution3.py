import cv2
import numpy as nump

#Exercice 1- part 1

image = cv2.imread('atlantis.jpg',1)

cv2.imshow('Frame',image)

cv2.imwrite('newImage.jpg',image)

#Exercice 3.
shape = image.shape
print ('Rows,Columns and channels')
print shape

pixel = image[200,150]
print ('Blue,Green and Red')
print pixel

blue = image[200,150,0]
green = image[200,150,1]
red = image[200,150,2]

print ('blue: %d') %blue
print ('Green: %d') %green
print ('Red: %d') %red


#Exercice 4
# Set pixels from 20x20 to 50x50
for i in range(20,50):
	for j in range(20,50):
		image[i,j] = [0,0,0]

roi = image[300:400, 200:300]
image[100:200,400:500] = roi

cv2.imshow('mod',image)
cv2.waitKey(0)
cv2.destroyAllWindows()